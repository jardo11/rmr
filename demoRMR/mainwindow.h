#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include<windows.h>
#include<iostream>
//#include<arpa/inet.h>
//#include<unistd.h>
//#include<sys/socket.h>
#include<sys/types.h>
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<vector>
#include "ckobuki.h"
#include "rplidar.h"
/*#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/core/utility.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/imgcodecs.hpp"*/


#define MAP_SIZE 12
#define MAP_DENSITY 100/8 // points in 1m //  8cm

namespace Ui {
class MainWindow;
}

typedef struct point {
    double x;
    double y;
}Tpoint;



typedef struct
{

    long double x;
    long double y;
    long double fi;
    long double x_old;
    long double y_old;
    long double fi_old;

}TLocation;

typedef struct
{
    unsigned short R;
    unsigned short L;
    unsigned short Gyro;
    unsigned short R_old;
    unsigned short L_old;
    unsigned short Gyro_old;
}TEnkoders;

typedef struct
{
    long double rot;
    long double old_rot;
    long double translation;
    long double old_translation;
    long double max_diff_translation;
    long double max_diff_rot;
}TSpeeds;


typedef struct
{
    int idx;
    double x_up;
    double y_up;
    double dist_up;
    double angle_up;
    double trajectory_up;
    double x_down;
    double y_down;
    double dist_down;
    double angle_down;
    double trajectory_down;
}TUloha2;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    bool useCamera;
  //  cv::VideoCapture cap;

    int actIndex;
    //    cv::Mat frame[3];

    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void robotprocess();
    void laserprocess();
    void processThisLidar(LaserMeasurement &laserData);

    void processThisRobot();
    HANDLE robotthreadHandle; // handle na vlakno
    DWORD robotthreadID;  // id vlakna
    static DWORD WINAPI robotUDPVlakno(void *param)
    {
        ((MainWindow*)param)->robotprocess();
        return 0;
    }
    HANDLE laserthreadHandle; // handle na vlakno
    DWORD laserthreadID;  // id vlakna
    static DWORD WINAPI laserUDPVlakno(void *param)
    {
        ((MainWindow*)param)->laserprocess();

        return 0;
    }
    //veci na broadcast laser
    struct sockaddr_in las_si_me, las_si_other,las_si_posli;

    int las_s,  las_recv_len;
    unsigned int las_slen;
    //veci na broadcast robot
    struct sockaddr_in rob_si_me, rob_si_other,rob_si_posli;

    int rob_s,  rob_recv_len;
    unsigned int rob_slen;

private slots:
    void on_pushButton_9_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_6_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_clicked();
    void getNewFrame();

    void on_pushButton_11_clicked();

    void on_pushButton_12_clicked();

    void on_doubleSpinBox_x_valueChanged(double arg1);

    void on_doubleSpinBox_y_valueChanged(double arg1);

    void on_pushButton_10_clicked();

    void on_pushButton_13_clicked();

    void on_doubleSpinBox_x_2_valueChanged(double arg1);

    void on_doubleSpinBox_y_2_valueChanged(double arg1);

    void on_pushButton_14_clicked();

//    void on_pushButton_7_clicked();

//    void on_pushButton_8_clicked();

    void on_uloha1_x_valueChanged(double arg1);

    void on_doubleSpinBox_y_6_valueChanged(double arg1);

private:
    JOYINFO joystickInfo;
    Ui::MainWindow *ui;
    void paintEvent(QPaintEvent *event);// Q_DECL_OVERRIDE;
    int updateLaserPicture;
    LaserMeasurement copyOfLaserData;
    std::string ipaddress;
    CKobuki robot;
    TKobukiData robotdata;
    int datacounter;
    int init;
    QTimer *timer;
    TLocation location;
    TEnkoders enkoders;
    void positioning();
    long double calculateGoalAngle(long double x, long double y);
    long double calculateDrivenLength();
    void calculateAxis();
    void calculateActualAngle();
    void savePreviousValues();
    void initEncodersValues();
    void floodFill(int row_dest, int col_dest);
    void uloha2();
    bool isActualGoalReachable();
    bool angleIsBetween(float angle, float start, float end);
    void findCorners();


    long double x;
    long double y;
    long double Kr;
    long double Kp;
    long double angle_treshold;
    long double distance_treshold;
    bool polohuj;
    bool mapuj;
    bool rotation;
    bool uloha_2 = false;
    std::vector<Tpoint> points;
    int size = MAP_SIZE*MAP_DENSITY+1;
    double map_density = 100.0/8.0;
    double desired_x_4 = 4.96, desired_y_4 = -0.24;
    int desired_row, desired_col;
    Tpoint goal_point_2 = {2, 3.6};
    std::vector<Tpoint> goal_points_2;
    double b = 0.36;// m
    TUloha2 uloha_2_data;
    Tpoint goal_point_uloha_1 = {0.5, 0};

    bool vypis =true;

    TSpeeds speed;
    int map[MAP_SIZE*MAP_DENSITY+1][MAP_SIZE*MAP_DENSITY+1] = { {} };

    std::ofstream myfile;


public slots:
     void setUiValues(double robotX,double robotY,double robotFi);
signals:
     void uiValuesChanged(double newrobotX,double newrobotY,double newrobotFi); ///toto nema telo


};

#endif // MAINWINDOW_H
