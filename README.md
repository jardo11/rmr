# Zadania RMR 1-4 #
## Prvotná konfigurácia buildu: ##
* pre správnu funkciu programu je ako potrebné nastaviť buildovací priečnok tak aby sa nachádzal v koreňovom adresári nášho repozitáru:<br />
![](/images/build_directory.png)<br />
/rmr je koreňový priečinok nášho repozitáru.

## Začiatok spustenia zhodný pre všetky úlohy: ##

* ako prvé je potrebné kliknúť na ikonu ![](/images/run.png) alebo použiť klavesovú skratku **ctrl+r**

* Po úspešnom zbuildení a spustení by sme mali vidieť nasledovné okno:<br />
![](/images/okno.png)

* Spustíme kobuki simulator:<br />
![](/images/kobuki.png)

* Posledným krokom je kliknutie na Start na našom užívatelskom okne:<br />
![](/images/start.png)

## Úloha č.1: ##
### Spustenie: ###
* Zobrazenie polohy robota vyzerá nasledovne:<br />
![](/images/lokalizacia.png)<br />

* Pre pridanie bodu do navigácie je potrebné nastaviť želanú polohu **x** a **y** a následne kliknúť na tlačidlo **1 uloha polohuj**. Na jednotlivé body sa polohuje postupne pričom bod ktorý bol zadaný ako prvý sa vykonáva ako prvý.<br />
![](/images/uloha_1.png)<br />
### Vysvetlenie logiky: ###
* vývojový diagram lokalizácie:<br />
![](/images/diagram_lokalizacia.png)<br />
* vývojový diagram polohovania (funkcia je volaná vždy ked prídu dáta z robota):<br />
![](/images/diagram_polohovanie.png)<br />


## Úloha č.2: ##
### Spustenie: ###
* V úlohe 2 je vhodné upraviť súbor priestor.txt ktorý sa nachádza v adresári kobuki nasledovne:<br /> 8 [0,0] [574.5,0] [574.5,460.5] [550.5,460.5] [550.5,471.5] [55.5,471.5] [55.5,431.5] [0,431.5] //obvod<br />
4 [110,151.5] [110,309] [114,309] [114,154.5]//3<br />
Po úprave vyzerá symulátor nasledovne:<br />
![](/images/kobuki_edited.png)
* Pred prvým polohovaním je potrebné kliknúť na tlačidlo 2 uloha.<br /> ![](/images/uloha_2.png)<br />
* Pre pridanie bodu do navigácie je potrebné nastaviť želanú polohu **x** a **y** a následne kliknúť na tlačidlo **uloz novy ciel**. Na jednotlivé body sa polohuje postupne pričom bod ktorý bol zadaný ako prvý sa vykonáva ako prvý.<br />

### Vysvetlenie logiky: ###
* vývojový diagram druhej úlohy:<br /> ![](/images/diagram_2uloha.png)<br />


## Úloha č.3: ##
### Spustenie: ###
* Pre spustenie mapovania je potrebné byť robotom na pozícii **[0, 0]** a následne kliknúť na tlačilo **3 uloha - mapuj.**<br />
![](/images/uloha_3.png)<br />
### Vysvetlenie logiky: ###
* Velkosť mapy sme si zvolili 12x12 metrov (je to dvojnásobok velkosti reálnej mapy, toto robíme pre to, aby sme zaručili to že mapovanie bude fungovať aj vtedy keď sa robot na začiatku nachádza inde ako v **[0, 0]**) a rozlíšenie mapy je na úrovni 8 centimetrov. 
* Vývojový diagram tretej úlohy:<br /> ![](/images/diagram_3uloha.png)<br />
## Úloha č.4: ##
### Spustenie: ###
* Pre pridanie bodu do navigácie je potrebné nastaviť želanú polohu **x** a **y** a následne kliknúť na tlačidlo **4 uloha - polohuj**. Na zadaný bod sa polohuje dovtedy kým daný bod robot nedosiahne alebo sa nastaví nový bod. V prípade že polohovanie nefunguje je potrebné zvoliť iný bod (nastavený bod nebol dostupný).<br />
![](/images/uloha_4.png)<br />
### Vysvetlenie logiky: ###
* Mapa získaná z tretej úlohy:<br />
![](/images/mapa.png)<br />
* Mapu získanú z tretej úlohy je potrebné zväčšiť minimálne o polomer robota. V našom prípade je to o dve bunky mapy. Zväčšená mapa vyzerá nasledovne:<br />
![](/images/mapa_zvacsena.png)<br />
*  Najskôr sme si nakopírovali zväčšenú mapu do dynamicky alokovaného 2d pola (to sme robili pre to aby sme nemuseli stále čítať mapu zo súboru). Následne sme vykonali flood fill pomocou 4-susednosti pričom sme začali od cielového bodu a  následne sme hladali také susedné bunky ktoré nie sú prekážka alebo už nie sú označené pomocou flood fill (tie bunky ktoré majú hodnotu rovnú 0). Toto robíme až do vtedy, dokým nenájdeme aktuálnu pozíciu na ktorej sa nachádzame. Výsledok získaný pomocou flood fill:<br />
![](/images/4uloha.png)<br />
* Výsledná cesta ktorou sa vydá robot (zobrazená na obrázku nižšie) získame tak že začneme o bodu kde sa aktuálne nacháza náš robot pričom hladáme nižšie číslo bunky ako to na ktorom sa aktuálne nachádzame. Ak sa vyberieme nejakým smerom tak tento smer dodržiavame do vtedy pokým platí to že budúca pozícia je nižšia ako aktuálna, ak táto podmienka neplatí nájddeme nový smer. Toto opakujeme do vtedy kým sa nedostaneme na bunku s hodnotou 2. Pri zmene smeru si vždy zapíšeme bod do navigacie, taktiež do navigácie zapíšeme bunku na ktorej je hodnota 2.<br />
![](/images/4uloha_cesta.png)<br />
* V súbore mapa.xlsx sú vizualizované všetky údaje z úlohy č.3 a č.4.